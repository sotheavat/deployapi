#!/bin/bash

file_path="$1";

cur_dir=$PWD;

update_option="$2";

#folder that point to react build folder or jar/war file

path_to_react_build_folder="/home/voth/Documents/deploy/react/build";

path_to_dashboard_file_folder="/opt/docker-app/ptassala/dashboard";

path_to_api_file_folder="/opt/docker-app/ptassala/api";

#folder that point to loacation of docker compose file api,react,dashboard

path_to_react_docker_compose_folder="/home/voth/Documents/deploy/react"

path_to_api_docker_compose_folder="/opt/docker-app/ptassala/"

path_to_dashboard_docker_compose_folder="/opt/docker-app/ptassala/dashboard"

#change this if your package your api as a jar file

api_file_full_name="ROOT.war"

#change this if your package your dashboard as a jar file

dashbaord_file_full_name="ROOT.war"

found="false";

temp="";

edc="1";

dc_1=""

dc_2="";

dc_3="";

execute_docker_compose(){

  local cur_dir=$PWD;

  if [[ "$edc" == "1" ]];
  
  then
    
    cd $path_to_react_docker_compose_folder ;

  elif  [[ "$edc" == "2" ]];
  
  then

    cd $path_to_dashboard_docker_compose_folder;

  else

    cd $path_to_api_docker_compose_folder;

  fi

  docker-compose $dc_1 $dc_2 $dc_3;

  cd $cur_dir;

}


clean_docker_images(){

  found="false";

  temp="";

  docker images | grep '<none>' | while read -r line ;
  do
	  for str in $line
	  do
		    if [[ "$str" =~ "<none>" ]];
	  	  then
          found="true";
		    fi
	  done
  if [[ "$found" =~ "true" ]] ;
  then
     if [[ "$temp" == "$line" ]];
    then        
      echo "found images...";
    else
      temp=($line);
    fi
  fi  
  for i in ${!temp[@]};
   do
     if [[ "$i" == "2" ]];
     then
       sudo docker rmi ${temp[$i]}; 
     fi  
   done
  done
}

export -f execute_docker_compose;

if [[ "$file_path" == "--help" ]];

then

  echo "Usage: redeploy [OPTION]";

  echo "";

  echo "Available Options :";
  
  echo " Option 1 : react";
  
  echo " Option 2 : dashboard";

  echo " Option 3 : api";

  echo " Option --help : help manual";

else
  
  if [[ $1 =~ ^/ ]];
  
  then
  
    file_path=$1;
  
  else
  
    file_path="$PWD/$1"; 
  
  fi
  
  if [[ "$update_option" == "1" ]];
  
  then

    echo "redeploying react ....";

    edc="1";

    dc_1="down";

    execute_docker_compose;

    dc_1="up";

    dc_2="-d";

    dc_3="--build";
  

    echo "removing old file from $path_to_react_build_folder/*"
    1
    unzip -o $file_path -d "$path_to_react_build_folder";
    1
    execute_docker_compose;

    echo "checking for old docker images...";
  
    clean_docker_images;
  
  elif [[ "$update_option" == "2" ]];
  
  then

    echo "redeploying dashboard...";

    edc="2";

    dc_1="down";

    execute_docker_compose;
    
    dc_1="up";

    dc_2="-d";

    dc_3="--build";
  
    sudo rm "$path_to_dashboard_file_folder/$dashbaord_file_full_name";
  
    sudo cp $file_path "$path_to_dashboard_file_folder/$dashbaord_file_full_name";
    
    execute_docker_compose;

    echo "checking for old docker images ...";

    clean_docker_images;
  
  elif [[ "$update_option" == "3" ]];
  
  then

    echo "redeploying api....";

    edc="3";

    dc_1="down";

    execute_docker_compose;

    dc_1="up";

    dc_2="-d";

    dc_3="--build"; 
  
    sudo rm "$path_to_api_file_folder/$api_file_full_name";
    
    sudo cp $file_path "$path_to_api_file_folder/$api_file_full_name";
    
    execute_docker_compose;

    echo "checking for old docker images...";
    
    clean_docker_images;
  
  else 
  
    echo "Invalid argumment: cammand should be : update new_file_path option";
  
    echo "Available Option :";
    
    echo " Option 1 : react";
    
    echo " Option 2 : dashboard";
    
    echo " Option 3 : api";
  
  fi

fi

cd $cur_dir;
