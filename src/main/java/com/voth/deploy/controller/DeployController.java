package com.voth.deploy.controller;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@RestController
@RequestMapping("/deploy")
public class DeployController {
    private Logger logger;
    private static final String SECRET_KEY = "mySuperSecretCodeForDeployMyApi";
    private static final String DEPLOY_API_FOLDER = " /home/$USER/update.sh /home/$USER/deploy/deployapi/file/";
    private static final String SUCCESS = "Success";
    private static final String SHELL = "/bin/bash";
    private static final String INVALID_ACTIVATION_CODE = "Invalid Activation Code";

    @PostMapping(value = "/{activationCode}/react",consumes = "multipart/form-data")
    public ResponseEntity<String> react(@RequestPart MultipartFile file, @PathVariable String activationCode) {
        if (activationCode.equalsIgnoreCase(SECRET_KEY)){
            try {
                saveFile(file);
                String[] cmd = {SHELL,"-c",DEPLOY_API_FOLDER+file.getOriginalFilename()+" 1"};
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new
                        InputStreamReader(process.getInputStream()));

                BufferedReader stdError = new BufferedReader(new
                        InputStreamReader(process.getErrorStream()));
                String s;
                while ((s = stdInput.readLine()) != null) {
                    logger.info(s);
                }
                while ((s = stdError.readLine()) != null) {
                    logger.info(s);
                }
                String[] commandOutput = {SHELL,"-c",DEPLOY_API_FOLDER+file.getOriginalFilename()};
                Runtime.getRuntime().exec(commandOutput);
            } catch (IOException e) {
                return new ResponseEntity<>("fail", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(SUCCESS, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(INVALID_ACTIVATION_CODE, HttpStatus.BAD_REQUEST);
        }
    }

    private void saveFile(@RequestPart MultipartFile file) {
        while (true){
            try {
                Files.copy(file.getInputStream(), Paths.get("file",file.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException n){
                if (new File(Paths.get("file").toString()).mkdirs()){
                    continue;
                }
            }
            break;
        }
    }

    @PostMapping(value = "/{activationCode}/api",consumes = "multipart/form-data")
    public ResponseEntity<String> api(@RequestPart MultipartFile file, @PathVariable String activationCode) {
        if (activationCode.equalsIgnoreCase(SECRET_KEY)){
            try {
                saveFile(file);
                String[] cmd = {SHELL,"-c",DEPLOY_API_FOLDER+file.getOriginalFilename()+" 3"};
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new
                        InputStreamReader(process.getInputStream()));

                BufferedReader stdError = new BufferedReader(new
                        InputStreamReader(process.getErrorStream()));
                String s;
                while ((s = stdInput.readLine()) != null) {
                    logger.info(s);
                }
                String[] commandOutput = {SHELL,"-c",DEPLOY_API_FOLDER+file.getOriginalFilename()};
                Runtime.getRuntime().exec(commandOutput);
                while ((s = stdError.readLine()) != null) {
                    logger.info(s);
                }
            } catch (IOException e) {
                return new ResponseEntity<>("fail", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(SUCCESS, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(INVALID_ACTIVATION_CODE, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/{activationCode}/dashboard",consumes = "multipart/form-data")
    public ResponseEntity<String> dashboard(@RequestPart MultipartFile file, @PathVariable String activationCode) {
        if (activationCode.equalsIgnoreCase(SECRET_KEY)){
            try {
                saveFile(file);
                String[] cmd = {SHELL,"-c",DEPLOY_API_FOLDER+file.getOriginalFilename()+" 2"};
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new
                        InputStreamReader(process.getInputStream()));
                String s;
                while ((s = stdInput.readLine()) != null) {
                    logger.info(s);
                }
                String[] commandOutput = {SHELL,"-c",DEPLOY_API_FOLDER+file.getOriginalFilename()};
                Runtime.getRuntime().exec(commandOutput);
            } catch (IOException e) {
                return new ResponseEntity<>("fail", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(SUCCESS, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(INVALID_ACTIVATION_CODE, HttpStatus.BAD_REQUEST);
        }
    }
}
