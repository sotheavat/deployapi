package com.voth.deploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeployapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeployapiApplication.class, args);
    }

}
