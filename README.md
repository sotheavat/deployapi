## Before you package This to Jar :

###    Check this piece of variable in Deploy Controller "DEPLOY_API_FOLDER"

-   Save Update.sh to your home directory.
-   run command "chmod +x update.sh"
-   Change "/home/$USER/deploy/deployapi/file/" to /home/$USER/PATH_TO_YOUR_DEPLOY_API_JAR_FOLDER/
-   Create Folder name "file" in YOUR_DEPLOY_API_JAR_FOLDER
-   Run jar file with command "java -jar deployapi-0.0.1-SNAPSHOT.jar"
